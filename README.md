# End To End Machine Learning Project

## Clone the github repository


## Create a virtual environment
- python3 -m venv venv

## Activate your virtual environment
- source venv/bin/activate (For Linux Users)

## Install all the requirements packages
- pip install -r requirements.txt
